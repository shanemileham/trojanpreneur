var express = require('express');
var router = express.Router();
var models  = require('../models');
var q = require('q');

// GET idea
router.get('/', function(req, res, next) {
    models.idea.findAll().then(function(ideas){
        res.status(200).json(ideas);
    });
});

// POST idea
router.post('/', function(req, res, next){
    var name = null, description = null;

	if (req.body.name){
        name = req.body.name;
    }
	if (req.body.description){
        description = req.body.description;
    }

	models.idea.create({
		name: name,
		description: description
	}).then(function(created_idea){
        res.status(201).json(created_idea);
    });
});

// GET idea/:ideaid
router.get('/:ideaid', function(req, res, next){
    models.idea.findOne({
        where: {
            id: req.params.ideaid
        }
    }).then(function(idea){
        res.status(200).json(idea);
    });
});

// POST idea/update/:ideaid
router.post('/update/:ideaid', function(req, res, next){
    var ideaid = req.params.ideaid,
		promise_name = null,
		promise_description = null;

	if (req.body.name){
        promise_name = models.idea.update({
            name: req.body.name
        },{
            where: {
                id: ideaid
            }
        });
    }

	if (req.body.description){
        promise_description = models.idea.update({
            description: req.body.description
        },{
            where: {
                id: ideaid
            }
        });
    }

	q.all([promise_name, promise_description]).then(function(){
        models.idea.findOne({
            where: {
                id: ideaid
            }
        }).then(function(updated_idea){
            res.status(200).json(updated_idea);
        });
    });
});

// POST idea/destroy/:ideaid
router.post('/destroy/:ideaid', function(req, res, next) {
    models.idea.findOne({
        where: {
            id: req.params.ideaid
        }
    }).then(function(idea){
        models.idea.destroy({
            where: {
                id: req.params.ideaid
            }
        }).then(function(){
            res.status(200).json(idea);
        });
    });
});

// GET idea/:ideaid/users
router.get('/:ideaid/users', function(req, res, next){
    models.idea.findOne({
        where: {
            id: req.params.ideaid
        }
    }).then(function(idea){
        idea.getUser().then(function(ideas){
            res.status(200).json(ideas);
        });
    });
});

// POST ideas/:ideaid/users
router.post(':ideaid/users', function(req, res, next){
    var promise_idea = null,
		promise_user = null;

    promise_idea = models.idea.findOne({
        where: {
            id: req.params.ideaid
        }
    });
    promise_user = models.user.findOne({
        where: {
            id: req.body.userid
        }
    });

    q.all([promise_idea, promise_user]).then(function(results){
        var idea = results[0],
		user = results[1];
        idea.addUser(user);
        res.status(201).json(user);
    });
});

// POST ideas/:ideaid/users/:userid/destroy
router.post('/:ideaid/users/:userid/destroy', function(req, res, next){
    var promise_idea = null,
		promise_user = null;

    promise_idea = models.idea.findOne({
        where: {
            id: req.params.ideaid
        }
    });
    promise_user = models.user.findOne({
        where: {
            id: req.body.userid
        }
    });

    q.all([promise_idea, promise_user]).then(function(results){
        var idea = results[0],
		user = results[1];
        idea.removeUser(user);
        res.status(200).json(user);
    });
});

module.exports = router;

