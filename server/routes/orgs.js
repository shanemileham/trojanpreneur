var express = require('express');
var router = express.Router();
var models  = require('../models');
var q = require('q');

// GET org
router.get('/', function(req, res, next) {
    models.org.findAll().then(function(orgs){
        res.status(200).json(orgs);
    });
});

// POST org
router.post('/', function(req, res, next){
    var fb_id = null, name = null, description = null;

	if (req.body.fb_id){
        fb_id = req.body.fb_id;
    }
	if (req.body.name){
        name = req.body.name;
    }
	if (req.body.description){
        description = req.body.description;
    }

	models.org.create({
		fb_id: fb_id,
		name: name,
		description: description
	}).then(function(created_org){
        res.status(201).json(created_org);
    });
});

// GET org/:orgid
router.get('/:orgid', function(req, res, next){
    models.org.findOne({
        where: {
            id: req.params.orgid
        }
    }).then(function(org){
        res.status(200).json(org);
    });
});

// POST org/update/:orgid
router.post('/update/:orgid', function(req, res, next){
    var orgid = req.params.orgid,
		promise_fb_id = null,
		promise_name = null,
		promise_description = null;

	if (req.body.fb_id){
        promise_fb_id = models.org.update({
            fb_id: req.body.fb_id
        },{
            where: {
                id: orgid
            }
        });
    }

	if (req.body.name){
        promise_name = models.org.update({
            name: req.body.name
        },{
            where: {
                id: orgid
            }
        });
    }

	if (req.body.description){
        promise_description = models.org.update({
            description: req.body.description
        },{
            where: {
                id: orgid
            }
        });
    }

	q.all([promise_fb_id, promise_name, promise_description]).then(function(){
        models.org.findOne({
            where: {
                id: orgid
            }
        }).then(function(updated_org){
            res.status(200).json(updated_org);
        });
    });
});

// POST org/destroy/:orgid
router.post('/destroy/:orgid', function(req, res, next) {
    models.org.findOne({
        where: {
            id: req.params.orgid
        }
    }).then(function(org){
        models.org.destroy({
            where: {
                id: req.params.orgid
            }
        }).then(function(){
            res.status(200).json(org);
        });
    });
});

module.exports = router;

