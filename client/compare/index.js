var app = angular.module('trojanpreneurapp', ['ngMaterial']);

app.controller('AppCtrl', ['$scope', '$http', '$location', '$mdSidenav', function($scope, $http, $location, $mdSidenav){

    //Facebook Script Initialize FB Object
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1397216853934063',
            xfbml      : true,
            version    : 'v2.3'
        });

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                console.log('Logged in.');
            }
            else {
                FB.login();
            }
        });

        //Login function
        $scope.login = function(){
            console.log("login() " + FB.getUserID());
            if (FB.getUserID() === ""){
                FB.login();
            }
        }

        $scope.loginText = "Log In";

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                $http.get("https://smileham-trojanpreneurapi.herokuapp.com/users")
                    .success(function(users) {
                        var fb_id = FB.getUserID();
                        users.some(function(user){
                            if (user.fb_id === fb_id){
                                $scope.loginText = user.name;
                                return 1;
                            }
                        });
                    });
            }
        });
    };

    //Add FB tag for use in page
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    //Navigation Bar Functions
    $scope.gotoUsers = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-users.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoOrgs = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-orgs.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoIdeas = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-ideas.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoEvents = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-events.html';
            }
            else {
                FB.login();
            }
        });
    }



}]);


app.controller('users', function($scope, $http, $mdDialog) {
    //Facebook Script Initialize FB Object
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1397216853934063',
            xfbml      : true,
            version    : 'v2.3'
        });

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                console.log('Logged in.');
                //                console.log(response);
            }
            else {
                FB.login();
            }
        });

        $http.get("https://smileham-trojanpreneurapi.herokuapp.com/users")
            .success(function(response) {
                //            console.log(response);
                var users = response;

                //Merge Events
                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        console.log('Logged in.');
                        users.forEach(function(user){
                            //                    console.log(user);
                            var fb = FB.api('/' + user.fb_id, function(response){
                                if (response && !response.error){
                                    //                            console.log(response);
                                    user.fb = response;

                                    var fb_pic = FB.api('/' + user.fb_id + '/picture?type=large', function(response){
                                        if (response && !response.error){
                                            //                                    console.log(response);
                                            user.fb.picture = response.data.url;

                                            //Load
                                            $http.get('',$scope.events);
                                        }
                                    });
                                }
                            });
                        });

                        $scope.users = users;
                        console.log(users);
                    }
                    else {
                        FB.login();
                    }
                });
            });

        //Login function
        $scope.login = function(){
            console.log("login() " + FB.getUserID());
            if (FB.getUserID() === ""){
                FB.login();
            }
        }

        $scope.loginText = "Log In";

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                $http.get("https://smileham-trojanpreneurapi.herokuapp.com/users")
                    .success(function(users) {
                        var fb_id = FB.getUserID();
                        users.some(function(user){
                            if (user.fb_id === fb_id){
                                $scope.loginText = user.name;
                                return 1;
                            }
                        });
                    });
            }
        });
    };

    //Add FB tag for use in page
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    //Navigation Bar Functions
    $scope.gotoUsers = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-users.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoOrgs = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-orgs.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoIdeas = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-ideas.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoEvents = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-events.html';
            }
            else {
                FB.login();
            }
        });
    }


    $scope.search = function(){
        $http.post('',$scope.user).success(successCallback)
    };

    $scope.deleteUser = function(id) {
        $http.post("https://smileham-trojanpreneurapi.herokuapp.com/users/destroy/" + id)
            .success(function(response) {
                console.log(response);
                location.reload();
            });
    };


    $scope.addUser = function() {
        console.log(FB);
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'add-user.tmpl.html'
        });
    };
    function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.submit = function(desc) {
            $mdDialog.hide();
            $http.post("https://smileham-trojanpreneurapi.herokuapp.com/users", {fb_id: FB.getUserID(),  description: desc})
                .success(function(response){
                    console.log(response);
                    location.reload();
                });
        };
    }
});

app.controller('events', function($scope, $http, $mdDialog) {

    //Facebook Script Initialize FB Object
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1397216853934063',
            xfbml      : true,
            version    : 'v2.3'
        });

        $http.get("https://smileham-trojanpreneurapi.herokuapp.com/events")
            .success(function(response) {
                //            console.log(response);
                var events = response;

                //Merge Events
                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        console.log('Logged in.');
                        events.forEach(function(event){
                            //                console.log(event);
                            FB.api('/' + event.fb_id, function(response){
                                if (response && !response.error){
                                    //                        console.log(response);
                                    event.fb = response;

                                    FB.api('/' + event.fb_id + '?fields=cover', function(response){
                                        if (response && !response.error){
                                            var fb_pic = response.cover.source;
                                            event.fb.picture = fb_pic;

                                            //Create nicely formatted time
                                            event.fb.time = event.fb.start_time.replace(/T/,' at ').replace(/:00-.*/,'');

                                            //Load
                                            $http.get('',$scope.events);
                                        }
                                    });

                                    $http.get('',$scope.events);
                                }
                            });
                        });

                        $scope.events = events;
                        console.log(events);
                    }
                    else {
                        FB.login();
                    }
                });
            });

        //Login function
        $scope.login = function(){
            console.log("login() " + FB.getUserID());
            if (FB.getUserID() === ""){
                FB.login();
            }
        }

        $scope.loginText = "Log In";

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                $http.get("https://smileham-trojanpreneurapi.herokuapp.com/users")
                    .success(function(users) {
                        var fb_id = FB.getUserID();
                        users.some(function(user){
                            if (user.fb_id === fb_id){
                                $scope.loginText = user.name;
                                return 1;
                            }
                        });
                    });
            }
        });
    };

    //Add FB tag for use in page
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));



    //Navigation Bar Functions
    $scope.gotoUsers = function(){
        console.log("test");
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-users.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoOrgs = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-orgs.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoIdeas = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-ideas.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoEvents = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-events.html';
            }
            else {
                FB.login();
            }
        });
    }



    $scope.search = function(){
        $http.post('',$scope.user).success(successCallback)
    };

    $scope.addEvent = function() {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'add-event.tmpl.html'
        });
    };

    $scope.deleteEvent = function(id) {
        $http.post("https://smileham-trojanpreneurapi.herokuapp.com/events/destroy/" + id)
            .success(function(response) {
                console.log(response);
                location.reload();
            });
    };

    function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.submit = function(eventid, name, description) {
            $mdDialog.hide(eventid);
            $http.post("https://smileham-trojanpreneurapi.herokuapp.com/events", {fb_id: eventid, name: name, description: description})
                .success(function(response){
                    console.log(response);
                    location.reload();
                });
        };
    }

});

app.controller('ideas', function($scope, $http, $mdDialog) {
    //Facebook Script Initialize FB Object
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1397216853934063',
            xfbml      : true,
            version    : 'v2.3'
        });

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                console.log('Logged in.');
            }
            else {
                FB.login();
            }
        });

        //Login function
        $scope.login = function(){
            console.log("login() " + FB.getUserID());
            if (FB.getUserID() === ""){
                FB.login();
            }
        }

        $scope.loginText = "Log In";

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                $http.get("https://smileham-trojanpreneurapi.herokuapp.com/users")
                    .success(function(users) {
                        var fb_id = FB.getUserID();
                        users.some(function(user){
                            if (user.fb_id === fb_id){
                                $scope.loginText = user.name;
                                return 1;
                            }
                        });
                    });
            }
        });
    };

    //Add FB tag for use in page
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    //Navigation Bar Functions
    $scope.gotoUsers = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-users.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoOrgs = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-orgs.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoIdeas = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-ideas.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoEvents = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-events.html';
            }
            else {
                FB.login();
            }
        });
    }



    $http.get("https://smileham-trojanpreneurapi.herokuapp.com/ideas")
        .success(function(response) {
            console.log(response);
            $scope.ideas = response;
        });

    $scope.search = function(){
        $http.post('',$scope.user).success(successCallback)
    };



    $scope.addIdea = function(id) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'add-idea.tmpl.html'
        });
    };

    $scope.deleteIdea = function(id) {
        $http.post("https://smileham-trojanpreneurapi.herokuapp.com/ideas/destroy/" + id)
            .success(function(response) {
                console.log(response);
                location.reload();
            });
    };

    $scope.adminIdea = function(id) {
        $mdDialog.show({
            controller: UpdateDialogController,
            templateUrl: 'upd-idea.tmpl.html'
        });
    };

    function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.submit = function(idea, desc,id) {
            $mdDialog.hide(idea);
            $http.post("https://smileham-trojanpreneurapi.herokuapp.com/ideas", {name: idea, description: desc})
                .success(function(response){
                    console.log(response);
                    location.reload();
                });
        };
    }

    function UpdateDialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.update = function(idea, desc, id) {
            $mdDialog.hide(idea);
            $http.post("https://smileham-trojanpreneurapi.herokuapp.com/ideas/update/"+id, {name: idea, description: desc})
                .success(function(response){
                    console.log(response);
                    location.reload();
                });
        };
    }

});

app.controller('orgs', function($scope, $http, $mdDialog) {
    //Facebook Script Initialize FB Object
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1397216853934063',
            xfbml      : true,
            version    : 'v2.3'
        });

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                console.log('Logged in.');
            }
            else {
                FB.login();
            }
        });

        //Login function
        $scope.login = function(){
            console.log("login() " + FB.getUserID());
            if (FB.getUserID() === ""){
                FB.login();
            }
        }

        $scope.loginText = "Log In";

        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                $http.get("https://smileham-trojanpreneurapi.herokuapp.com/users")
                    .success(function(users) {
                        var fb_id = FB.getUserID();
                        users.some(function(user){
                            if (user.fb_id === fb_id){
                                $scope.loginText = user.name;
                                return 1;
                            }
                        });
                    });
            }
        });
    };

    //Add FB tag for use in page
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    //Navigation Bar Functions
    $scope.gotoUsers = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-users.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoOrgs = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-orgs.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoIdeas = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-ideas.html';
            }
            else {
                FB.login();
            }
        });
    }
    $scope.gotoEvents = function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                window.location.href = 'directory-events.html';
            }
            else {
                FB.login();
            }
        });
    }


    $http.get("https://smileham-trojanpreneurapi.herokuapp.com/orgs")
        .success(function(response) {
            console.log(response);
            $scope.orgs = response;
        });

    $scope.search = function(){
        $http.post('',$scope.user).success(successCallback)
    };

    $scope.addOrg = function() {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'add-org.tmpl.html'
        });
    };

    $scope.adminOrg = function() {
        $mdDialog.show({
            controller: AdminDialogController,
            templateUrl: 'upd-org.tmpl.html'
        });
    };

    $scope.deleteOrg = function(id) {
        $http.post("https://smileham-trojanpreneurapi.herokuapp.com/orgs/destroy/" + id)
            .success(function(response) {
                console.log(response);
                location.reload();
            });
    };

    function AdminDialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.submit = function(org, desc,id) {
            $mdDialog.hide(org);
            $http.post("https://smileham-trojanpreneurapi.herokuapp.com/orgs/update/"+ id, {name: org, description: desc})
                .success(function(response){
                    console.log(response);
                    location.reload();
                });
        };
    }

    function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.submit = function(org, desc) {
            $mdDialog.hide(org);
            $http.post("https://smileham-trojanpreneurapi.herokuapp.com/orgs", {name: org, description: desc})
                .success(function(response){
                    console.log(response);
                    location.reload();
                });
        };
    }

});


app.controller('addUser', ['$scope','$http',function($scope,$http) {
    $scope.master = {description:""};

    $scope.reset = function() {
        $scope.event = angular.copy($scope.master);
    };
    $scope.reset();

    $scope.formSubmit = function(){
        $http.post("https://smileham-trojanpreneurapi.herokuapp.com/users", $scope.event)
            .success(function(response){
                console.log(response);
            });
    };

    $scope.formSubmit();
}]);

app.controller('addIdea', ['$scope','$http',function($scope,$http) {
    $scope.master = {name:"", description:""};

    $scope.reset = function() {
        $scope.event = angular.copy($scope.master);
    };
    $scope.reset();

    $scope.formSubmit = function(){
        $http.post("https://smileham-trojanpreneurapi.herokuapp.com/ideas", $scope.event)
            .success(function(response){
                console.log(response);
            });
    };

    $scope.formSubmit();
}]);





app.config(function($mdThemingProvider) {

    $mdThemingProvider.definePalette('cardinal', {
        '50': '000000',
        '100': '260000',
        '200': '400000',
        '300': '590000',
        '400': '730000',
        '500': '8c0000',
        '600': '990000', //This is the cardinal color
        '700': 'a60d0d',
        '800': 'bf2626',
        '900': 'd94040',
        'A100': 'f25959',
        'A200': 'ff7373',
        'A400': 'ffa6a6',
        'A700': 'ffffff',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
        // on this palette should be dark or light
        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
            '50', '300', '400', 'A100'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
    });

    $mdThemingProvider.definePalette('gold', {
        '50': '592600',
        '100': '8c5900',
        '200': 'a67300',
        '300': 'bf8c00',
        '400': 'd9a600',
        '500': 'f2bf00',
        '600': 'ffcc00', //This is the gold color
        '700': 'ffd90d',
        '800': 'fff226',
        '900': 'ffff40',
        'A100': 'ffff59',
        'A200': 'ffff73',
        'A400': 'ffffa6',
        'A700': 'ffffff',
        'contrastDefaultColor': 'dark',    // whether, by default, text (contrast)
        // on this palette should be dark or light
        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
            '200', '300', '400', 'A100'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
    });



    $mdThemingProvider.theme('default')
        .primaryPalette('cardinal', {
            'default': '600',
            'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
            'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
            'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
        })
        // If you specify less than all of the keys, it will inherit from the
        // default shades
        .accentPalette('gold', {
            'default': '600' // use shade 200 for default, and keep all other shades the same
        });
});