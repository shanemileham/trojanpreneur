var express = require('express');
var router = express.Router();
var models  = require('../models');
var q = require('q');

// GET tag
router.get('/', function(req, res, next) {
    models.tag.findAll().then(function(tags){
        res.status(200).json(tags);
    });
});

// POST tag
router.post('/', function(req, res, next){
    var text = null;

	if (req.body.text){
        text = req.body.text;
    }

	models.tag.create({
		text: text
	}).then(function(created_tag){
        res.status(201).json(created_tag);
    });
});

// GET tag/:tagid
router.get('/:tagid', function(req, res, next){
    models.tag.findOne({
        where: {
            id: req.params.tagid
        }
    }).then(function(tag){
        res.status(200).json(tag);
    });
});

// POST tag/update/:tagid
router.post('/update/:tagid', function(req, res, next){
    var tagid = req.params.tagid,
		promise_text = null;

	if (req.body.text){
        promise_text = models.tag.update({
            text: req.body.text
        },{
            where: {
                id: tagid
            }
        });
    }

	q.all([promise_text]).then(function(){
        models.tag.findOne({
            where: {
                id: tagid
            }
        }).then(function(updated_tag){
            res.status(200).json(updated_tag);
        });
    });
});

// POST tag/destroy/:tagid
router.post('/destroy/:tagid', function(req, res, next) {
    models.tag.findOne({
        where: {
            id: req.params.tagid
        }
    }).then(function(tag){
        models.tag.destroy({
            where: {
                id: req.params.tagid
            }
        }).then(function(){
            res.status(200).json(tag);
        });
    });
});

// GET tag/:tagid/users
router.get('/:tagid/users', function(req, res, next){
    models.tag.findOne({
        where: {
            id: req.params.tagid
        }
    }).then(function(tag){
        tag.getUser().then(function(tags){
            res.status(200).json(tags);
        });
    });
});

// POST tags/:tagid/users
router.post(':tagid/users', function(req, res, next){
    var promise_tag = null,
		promise_user = null;

    promise_tag = models.tag.findOne({
        where: {
            id: req.params.tagid
        }
    });
    promise_user = models.user.findOne({
        where: {
            id: req.body.userid
        }
    });

    q.all([promise_tag, promise_user]).then(function(results){
        var tag = results[0],
		user = results[1];
        tag.addUser(user);
        res.status(201).json(user);
    });
});

// POST tags/:tagid/users/:userid/destroy
router.post('/:tagid/users/:userid/destroy', function(req, res, next){
    var promise_tag = null,
		promise_user = null;

    promise_tag = models.tag.findOne({
        where: {
            id: req.params.tagid
        }
    });
    promise_user = models.user.findOne({
        where: {
            id: req.body.userid
        }
    });

    q.all([promise_tag, promise_user]).then(function(results){
        var tag = results[0],
		user = results[1];
        tag.removeUser(user);
        res.status(200).json(user);
    });
});

module.exports = router;

