/*jslint node: true, nomen: true, sloppy: true, white: true*/
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');

var app = express();
//var server = app.listen(8081, function() {
//    console.log(new Date().toISOString() + ": server started on port 8081");
//});


//POST body-parser setup
var bodyParser = require('body-parser');
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

// Add headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});


//Passport stuff
app.use(session({secret: 'ssshhhhh'}));
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(function(user, done) {
    console.log("SERIALIZING USER");
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    console.log("DESERIALIZING USER");
    done(null, user);
});
//function ensureAuthenticated(req, res, next) {
//  if (req.isAuthenticated()) { return next(); }
//  res.redirect('/login');
//}


//Routes
var routes = require('./routes/index');
app.use('/', routes);
var auth = require('./routes/auth');
app.use('/auth', auth);
var events = require('./routes/events');
app.use('/events', events);
var ideas = require('./routes/ideas');
app.use('/ideas', ideas);
var users = require('./routes/users');
app.use('/users', users);
var orgs = require('./routes/orgs');
app.use('/orgs', orgs);
var tags = require('./routes/tags');
app.use('/tags', tags);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
