var Sequelize = require('sequelize');
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);

module.exports = function (sequelize, DataTypes) {
    'use strict';
    var user = sequelize.define('user', {
		fb_id: {
            type: Sequelize.STRING,
            allowNull: true
        },
		fb_token: {
            type: Sequelize.STRING,
            allowNull: true
        },
		name: {
            type: Sequelize.STRING,
            allowNull: true
        },
		description: {
            type: Sequelize.STRING,
            allowNull: true
        }
	},{
        tableName: 'user',
        underscored: true,
        paranoid: true,
        classMethods: {
            associate: function(models){
				user.belongsToMany(models.idea, {through: 'idea_user'});
				user.belongsToMany(models.tag, {through: 'user_tag'});
            }
        }
    });
    return user;
};