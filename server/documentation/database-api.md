# Database API

## event
* GET event
* POST event
* GET event/:eventid
* POST event/update/:eventid
* POST event/destroy/:eventid


## idea
* GET idea
* POST idea
* GET idea/:ideaid
* POST idea/update/:ideaid
* POST idea/destroy/:ideaid
* GET idea/:ideaid/users
* POST ideas/:ideaid/users
* POST ideas/:ideaid/users/:userid/destroy


## org
* GET org
* POST org
* GET org/:orgid
* POST org/update/:orgid
* POST org/destroy/:orgid


## tag
* GET tag
* POST tag
* GET tag/:tagid
* POST tag/update/:tagid
* POST tag/destroy/:tagid
* GET tag/:tagid/users
* POST tags/:tagid/users
* POST tags/:tagid/users/:userid/destroy


## user
* GET user
* POST user
* GET user/:userid
* POST user/update/:userid
* POST user/destroy/:userid
* GET user/:userid/ideas
* POST users/:userid/ideas
* POST users/:userid/ideas/:ideaid/destroy
* GET user/:userid/tags
* POST users/:userid/tags
* POST users/:userid/tags/:tagid/destroy
