/*jslint node: true, nomen: true, sloppy: true, white: true*/

var express = require('express');
var router = express.Router();
var models  = require('../models');
var q = require('q');
var http = require('http');
var https = require('https');

// FB Passport Setup
var passport = require('passport')
, FacebookStrategy = require('passport-facebook').Strategy;
var FACEBOOK_APP_ID = 1397216853934063;
var FACEBOOK_APP_SECRET = "e605af10d832740d68f4fea308e06958";
var ACCESS_TOKEN = null;
var CALLBACK_URL = "https://smileham-trojanpreneurapi.herokuapp.com/auth/facebook/callback";
//var CALLBACK_URL = "http://localhost:3000/auth/facebook/callback";


var session;

//Passport Authentication
passport.use(new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: CALLBACK_URL
}, function(accessToken, refreshToken, profile, done) {
    console.log(accessToken);
    ACCESS_TOKEN = accessToken;
    //    session.fb_auth = accessToken;

    //Add user to db and update auth token
    models.user.findOrCreate({
        where: {
            fb_id: profile._json.id
        }
    }).then(function(user){
        models.user.update({
            fb_token: accessToken,
            name: profile.name.givenName + ' ' + profile.name.familyName,
            description: "desc"
        },{
            where: {
                fb_id: profile._json.id
            }
        }).then(function(){
            return done(null, user);
        });
    });
    //return done(null, profile);

}));

//Obtain Authentication Token
router.get('/facebook', passport.authenticate('facebook'));
router.get('/facebook/callback', 
           passport.authenticate('facebook', { successRedirect: 'http://localhost:8888/classes/itp460/trojanpreneur/client/',
                                              failureRedirect: 'http://localhost:8888/classes/itp460/trojanpreneur/client/' }));

//Ensure Authentication via promise
function ensureAuthenticated(req, res, next) {
    var dfd = q.defer();
    if (req.isAuthenticated()) {
        console.log("req.isAuthenticated");
        dfd.resolve(true);
    }else{
        console.log("!req.isAuthenticated");
        res.redirect('/auth/facebook');
        dfd.resolve(false);
    }
    return dfd.promise;
}


function mergeUser(req, res, next, user){
    console.log("Merge User");
    console.log("auth token: " + ACCESS_TOKEN);

    var dfd = q.defer();

    ensureAuthenticated(req, res, next).then(function(auth){
        if (auth){
            console.log("authenticated!");
            console.log("auth token: " + ACCESS_TOKEN);

            https.get('https://graph.facebook.com/' + user.fb_id + '?access_token=' + ACCESS_TOKEN, function(response) {
                var str = '';
                response.on('data', function(chunk){
                    str += chunk;
                });

                response.on('end', function(){
                    var fb_event = JSON.parse(str);
                    user.dataValues.fb = fb_event;

                    http.get('http://graph.facebook.com/' + user.fb_id + '/picture?type=large', function(response) {
                        user.dataValues.fb.picture = response.headers.location;
                        dfd.resolve(user);
                    });
                });
            });

        }else{
            console.log("not authenticated. redirecting to main page");
            dfd.resolve(user);
        }
    });

    return dfd.promise;
}

function mergeEvent(req, res, next, event){
    console.log("Merge Event");

    var dfd = q.defer();

    ensureAuthenticated(req, res, next).then(function(auth){
        if (auth){
            console.log("authenticated!");
            console.log("auth token: " + ACCESS_TOKEN);
            https.get('https://graph.facebook.com/' + event.fb_id + '?access_token=' + ACCESS_TOKEN, function(response) {
                var str = '';
                response.on('data', function(chunk){
                    str += chunk;
                });

                response.on('end', function(){
                    var fb_event = JSON.parse(str);
                    event.dataValues.fb = fb_event;
                    dfd.resolve(event);
                });
            });
        }else{
            console.log("not authenticated. redirecting to main page");
            dfd.resolve(event);
        }
    });

    return dfd.promise;
}

function mergeUsers(req, res, next, users){
    console.log("Merge Users");

    var dfd = q.defer();

    ensureAuthenticated(req, res, next).then(function(auth){
        var promises = [];

        if (auth){
            console.log("authenticated!");
            console.log("auth token: " + ACCESS_TOKEN);

            var count = 0;

            for (var iUser in users){
                var user = users[iUser];

                var promise = q.defer();
                promises.push(promise);

                https.get('https://graph.facebook.com/' + user.fb_id + '?access_token=' + ACCESS_TOKEN + "&pos=" + iUser, function(response) {
//                    console.log(response.req.path);
                    var pos = response.req.path.replace(/.*&pos=/, '');
//                    console.log(pos);

                    var str = '';
                    response.on('data', function(chunk){
                        str += chunk;
                    });

                    response.on('end', function(){
                        http.get('http://graph.facebook.com/' + users[pos].fb_id + '/picture?type=large', function(responsePic) {
                            var fb_user = JSON.parse(str);
                            users[pos].dataValues.fb = fb_user;
                            console.log(responsePic.headers.location);
                            users[pos].dataValues.fb.picture = responsePic.headers.location;
                            
                            promises[pos].resolve(users[pos].dataValues);

                            count = count + 1;
                            if (count === users.length){
                                dfd.resolve(users);
                            }
                        });
                    });
                });

            }
        }else{
            console.log("Not authenticated. Redirecting to main page.");
            dfd.resolve(users);
        }
    });

    return dfd.promise;
}

function mergeEvents(req, res, next, events){
    console.log("Merge Events");

    var dfd = q.defer();

    ensureAuthenticated(req, res, next).then(function(auth){
        var promises = [];

        if (auth){
            console.log("authenticated!");
            console.log("auth token: " + ACCESS_TOKEN);

            var count = 0;

            for (var iEvent in events){
                var event = events[iEvent];

                var promise = q.defer();
                promises.push(promise);

                https.get('https://graph.facebook.com/' + event.fb_id + '?access_token=' + ACCESS_TOKEN + "&pos=" + iEvent, function(response) {
                    console.log(response.req.path);
                    var pos = response.req.path.replace(/.*&pos=/, '');
                    console.log(pos);


                    var str = '';
                    response.on('data', function(chunk){
                        str += chunk;
                    });

                    response.on('end', function(){
                        var fb_event = JSON.parse(str);
                        events[pos].dataValues.fb = fb_event;
                        promises[pos].resolve(events[pos].dataValues);

                        count = count + 1;
                        if (count === events.length){
                            dfd.resolve(events);
                        }
                    });
                });

            }
        }else{
            console.log("Not authenticated. Redirecting to main page.");
            dfd.resolve(events);
        }
    });

    return dfd.promise;
}


//Exports
module.exports = router;
module.exports.mergeUser = mergeUser;
module.exports.mergeEvent = mergeEvent;
module.exports.mergeUsers = mergeUsers;
module.exports.mergeEvents = mergeEvents;
