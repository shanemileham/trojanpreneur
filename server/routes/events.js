var express = require('express');
var router = express.Router();
var models  = require('../models');
var q = require('q');

var auth = require('../routes/auth.js');

// GET event
router.get('/', function(req, res, next) {
    res.status(200).json(
[{"id":1,"fb_id":"1409346876040957","name":"ted x","description":"ted x desc","created_at":"0000-00-00 00:00:00","updated_at":"0000-00-00 00:00:00","deleted_at":null,"fb":{"description":"TEDxTrousdale serves to quench the curiosities and expand the interests of all USC students regardless of major, age or background. This year, we aim to showcase the passions, insights, and accomplishments of USC students from all corners of campus who have one thing in common: the courage to transcend boundaries and create a meaningful difference. Covering an array of diverse topics, their stories will force you to question existing paradigms and convictions, inspire you to change, and stretch your imagination beyond its ordinary limits.\n\nDATE: Saturday, April 25, 2015\nTIME: Program runs from 10:00am-1:30pm. Check-in at 9:30am (FREE coffee and lunch will be served)*\nLOCATION: Tommy’s Place (on USC campus) \n\nRead more about our speakers on our official event page! https://www.ted.com/tedx/events/15449\n\n*All attendees must check in by 9:45am and stay the duration of the entire event! We will begin admitting from the standby line starting at 9:50am.","end_time":"2015-04-25T14:00:00-0700","is_date_only":false,"name":"TEDxTrousdale 2015: Color Your Perspective","owner":{"category":"Community","name":"TEDxTrousdale","id":"567695579994596"},"privacy":"OPEN","start_time":"2015-04-25T10:00:00-0700","timezone":"America/Los_Angeles","updated_time":"2015-04-20T06:05:22+0000","id":"1409346876040957"}},{"id":10,"fb_id":"821788324580840","name":"rapgenius cofounder","description":null,"created_at":"0000-00-00 00:00:00","updated_at":"0000-00-00 00:00:00","deleted_at":null,"fb":{"description":"Join us for an evening of entrepreneurship and music with Genius Co-Founder Mahbod Moghadam! \n\nGenius allows users to provide annotations of song lyrics, news stories, poetry, and pretty much any form of text. Mahbod is notorious for his controversial and unfiltered comments - always making for an interesting and engaging time. There will definitely be a Q&A opportunity as well as other interactive components to the evening.\n\nGenius has raised over $56 million from Notable investors including:Ycombinator, Andreesen Horowitz, Dan Gilbert, Alexis Ohanian, Nasir “NAS” Jones, and Betaworks. It is currently the #1 lyrics website in the world and is growing in popularity as Genius forms partnerships with a wide variety of artists. \n\ngenius.com","end_time":"2015-04-22T22:00:00-0700","is_date_only":false,"name":"TAMID Hosts Rap-Genius Co-Founder Mahbod Moghadam","owner":{"id":"10205921405330235","name":"Danielle Fallon"},"privacy":"OPEN","start_time":"2015-04-22T20:00:00-0700","timezone":"America/Los_Angeles","updated_time":"2015-04-07T02:23:24+0000","id":"821788324580840"}},{"id":11,"fb_id":"535712343235106","name":"lavalab demo night","description":null,"created_at":"0000-00-00 00:00:00","updated_at":"0000-00-00 00:00:00","deleted_at":null,"fb":{"description":"For an entire semester, our eight undergraduate startups at LavaLab, USC's product design incubator program, have been building amazing products spanning from an RFID wristband analytics platform to mobile apps that transform the way we travel, and now it's finally time for them to show off.\n\nWe've invited a crowd of our mentors, investors, and exceptional students to attend, mingle, and check out the progress our teams have made. We hope you are able to join us for an evening of incredible demos and incredible people.\n\nIn order to participate in the event, you MUST register on Eventbrite below:\nhttps://www.eventbrite.com/e/lavalab-demo-night-tickets-16268849567\n\nDoors open at 7:30, the event starts at 8:00, and pitches begin at 9:00.\n\nFor students looking to apply and join LavaLab, Demo Night is a great event for you to see what we are all about. Make sure to check us out at usclavalab.org","is_date_only":false,"name":"LavaLab Demo Night: Spring 15","owner":{"category":"Organization","name":"LavaLab","id":"113822875452473"},"privacy":"OPEN","start_time":"2015-04-28T20:00:00-0700","timezone":"America/Los_Angeles","updated_time":"2015-04-22T06:12:08+0000","id":"535712343235106"}},{"id":12,"fb_id":"1170882346261070","name":"dfa open studio","description":null,"created_at":"0000-00-00 00:00:00","updated_at":"0000-00-00 00:00:00","deleted_at":null,"fb":{"description":"Interested in learning more about design thinking + social impact? Curious to see what Design for America USC has been up to this year?\n\nCome to our open studio night for the Spring '15 semester!  \nFor an entire semester, our 4 interdisciplinary project teams have been venturing out into various neighborhoods such as Watts, Skid Row, South Central, and the USC campus to design and build meaningful products and services for the community. You will be able to hear from all 4 of our project teams, give them feedback on their designs, and just chat about design thinking + social impact opportunities both on and off of the USC campus. You'll also be able to interact with some awesome installations.\n\nFor food RSVP here: dfaopenstudionightspring15.splashthat.com\n\nWe are inviting design industry professionals, our community partners, social impact professionals, and faculty to see the progress our teams have made. We hope you are able to join us for a fun and interactive exhibition night of social impact projects and peoples. \n\nAnd if that's not enough of an incentive, hey, there will be food :)\n\nHope to see y'all there!\n\nFor more info on Design for America USC, check us out at designforamericausc.com. For prospective students looking to apply and join DFA, Open Studio Night is a great event for you to see what we are all about.","end_time":"2015-04-29T22:00:00-0700","is_date_only":false,"name":"DFA Open Studio Night: Spring '15","owner":{"category":"Community","name":"Design for America - USC","id":"538433292891557"},"privacy":"OPEN","start_time":"2015-04-29T20:00:00-0700","timezone":"America/Los_Angeles","updated_time":"2015-04-12T20:56:08+0000","id":"1170882346261070"}}]);
    
    models.event.findAll().then(function(events){
        auth.mergeEvents(req, res, next, events).then(function(mergedEvents){
            res.status(200).json(mergedEvents);
        });
    });
});

// POST event
router.post('/', function(req, res, next){
    var fb_id = null, name = null, description = null;

	if (req.body.fb_id){
        fb_id = req.body.fb_id;
    }
	if (req.body.name){
        name = req.body.name;
    }
	if (req.body.description){
        description = req.body.description;
    }

	models.event.create({
		fb_id: fb_id,
		name: name,
		description: description
	}).then(function(created_event){
        res.status(201).json(created_event);
    });
});

// GET event/:eventid
router.get('/:eventid', function(req, res, next){
    models.event.findOne({
        where: {
            id: req.params.eventid
        }
    }).then(function(event){
        auth.mergeEvent(req, res, next, event).then(function(mergedEvent){
            res.status(200).json(mergedEvent);
        });
    });
});

// POST event/update/:eventid
router.post('/update/:eventid', function(req, res, next){
    var eventid = req.params.eventid,
		promise_fb_id = null,
		promise_name = null,
		promise_description = null;

	if (req.body.fb_id){
        promise_fb_id = models.event.update({
            fb_id: req.body.fb_id
        },{
            where: {
                id: eventid
            }
        });
    }

	if (req.body.name){
        promise_name = models.event.update({
            name: req.body.name
        },{
            where: {
                id: eventid
            }
        });
    }

	if (req.body.description){
        promise_description = models.event.update({
            description: req.body.description
        },{
            where: {
                id: eventid
            }
        });
    }

	q.all([promise_fb_id, promise_name, promise_description]).then(function(){
        models.event.findOne({
            where: {
                id: eventid
            }
        }).then(function(updated_event){
            res.status(200).json(updated_event);
        });
    });
});

// POST event/destroy/:eventid
router.post('/destroy/:eventid', function(req, res, next) {
    models.event.findOne({
        where: {
            id: req.params.eventid
        }
    }).then(function(event){
        models.event.destroy({
            where: {
                id: req.params.eventid
            }
        }).then(function(){
            res.status(200).json(event);
        });
    });
});

module.exports = router;

