var express = require('express');
var router = express.Router();
var models  = require('../models');
var q = require('q');

var auth = require('../routes/auth.js');

// GET user
router.get('/', function(req, res, next) {
    res.status(200).json([
  {
    "id": 1,
    "fb_id": "10204989182542996",
    "fb_token": "CAAT2wuZAAvZB8BAFAsO7V4sSUzeAhjRbU8bZCCrwkGZAjp9BaZC9alAzEzOYXZBnUBsUTZBusGpY0i90bT0n7cnn9vS2RxuBNeDXQfWAWPgKvZAzty5yr7gVX6vRjqo5Pf61nZAAraHmkpXpSZBIh28AqrFkKjeZC1c4IZCQPrlKo3DNSm6Q9zN80jZBjFhy62L9xIwvS5JHKORiZAvh3zVaX8BPnK",
    "name": "Rebecca Casey",
    "description": "Rebecca is a Chemical Engineering major at USC pursuing a minor in Web Technologies and Applications. She is a hiking and travel enthusiast with a passion for web design.",
    "created_at": "2015-04-21T22:25:12.000Z",
    "updated_at": "2015-04-23T06:39:44.000Z",
    "deleted_at": null,
    "fb": {
      "id": "10204989182542996",
      "first_name": "Shane",
      "gender": "male",
      "last_name": "Mileham",
      "link": "https://www.facebook.com/app_scoped_user_id/10204989182542996/",
      "locale": "en_US",
      "name": "Shane Mileham",
      "timezone": -7,
      "updated_time": "2015-04-15T23:25:33+0000",
      "verified": true,
      "picture": "images/rebecca.png"
    }
  },
  {
    "id": 1,
    "fb_id": "10204989182542996",
    "fb_token": "CAAT2wuZAAvZB8BAFAsO7V4sSUzeAhjRbU8bZCCrwkGZAjp9BaZC9alAzEzOYXZBnUBsUTZBusGpY0i90bT0n7cnn9vS2RxuBNeDXQfWAWPgKvZAzty5yr7gVX6vRjqo5Pf61nZAAraHmkpXpSZBIh28AqrFkKjeZC1c4IZCQPrlKo3DNSm6Q9zN80jZBjFhy62L9xIwvS5JHKORiZAvh3zVaX8BPnK",
    "name": "Nittai Malchin",
    "description": "Nittai is a pursuing his Business Administration degree at USC and has experience in business development and project management. He is excited about bringing the community of entrepreneurs closer at USC through Trojanpreneur.",
    "created_at": "2015-04-21T22:25:12.000Z",
    "updated_at": "2015-04-23T06:39:44.000Z",
    "deleted_at": null,
    "fb": {
      "id": "10204989182542996",
      "first_name": "Shane",
      "gender": "male",
      "last_name": "Mileham",
      "link": "https://www.facebook.com/app_scoped_user_id/10204989182542996/",
      "locale": "en_US",
      "name": "Shane Mileham",
      "timezone": -7,
      "updated_time": "2015-04-15T23:25:33+0000",
      "verified": true,
      "picture": "images/nittai.png"
    }
  },
  {
    "id": 1,
    "fb_id": "10204989182542996",
    "fb_token": "CAAT2wuZAAvZB8BAFAsO7V4sSUzeAhjRbU8bZCCrwkGZAjp9BaZC9alAzEzOYXZBnUBsUTZBusGpY0i90bT0n7cnn9vS2RxuBNeDXQfWAWPgKvZAzty5yr7gVX6vRjqo5Pf61nZAAraHmkpXpSZBIh28AqrFkKjeZC1c4IZCQPrlKo3DNSm6Q9zN80jZBjFhy62L9xIwvS5JHKORiZAvh3zVaX8BPnK",
    "name": "Shane Mileham",
    "description": "desc",
    "created_at": "2015-04-21T22:25:12.000Z",
    "updated_at": "2015-04-23T06:39:44.000Z",
    "deleted_at": null,
    "fb": {
      "id": "10204989182542996",
      "first_name": "Shane",
      "gender": "male",
      "last_name": "Mileham",
      "link": "https://www.facebook.com/app_scoped_user_id/10204989182542996/",
      "locale": "en_US",
      "name": "Shane Mileham",
      "timezone": -7,
      "updated_time": "2015-04-15T23:25:33+0000",
      "verified": true,
      "picture": "images/shane.png"
    }
  },
  {
    "id": 1,
    "fb_id": "10204989182542996",
    "fb_token": "CAAT2wuZAAvZB8BAFAsO7V4sSUzeAhjRbU8bZCCrwkGZAjp9BaZC9alAzEzOYXZBnUBsUTZBusGpY0i90bT0n7cnn9vS2RxuBNeDXQfWAWPgKvZAzty5yr7gVX6vRjqo5Pf61nZAAraHmkpXpSZBIh28AqrFkKjeZC1c4IZCQPrlKo3DNSm6Q9zN80jZBjFhy62L9xIwvS5JHKORiZAvh3zVaX8BPnK",
    "name": "Brian Whitney",
    "description": "Brian is a jazz saxophonist and web designer at the University of Southern California. He specializes in quality assurance and will be starting full-time employment with Accenture Consulting in Seattle later this year.",
    "created_at": "2015-04-21T22:25:12.000Z",
    "updated_at": "2015-04-23T06:39:44.000Z",
    "deleted_at": null,
    "fb": {
      "id": "10204989182542996",
      "first_name": "Shane",
      "gender": "male",
      "last_name": "Mileham",
      "link": "https://www.facebook.com/app_scoped_user_id/10204989182542996/",
      "locale": "en_US",
      "name": "Shane Mileham",
      "timezone": -7,
      "updated_time": "2015-04-15T23:25:33+0000",
      "verified": true,
      "picture": "images/brianw.png"
    }
  },
  {
    "id": 1,
    "fb_id": "10204989182542996",
    "fb_token": "CAAT2wuZAAvZB8BAFAsO7V4sSUzeAhjRbU8bZCCrwkGZAjp9BaZC9alAzEzOYXZBnUBsUTZBusGpY0i90bT0n7cnn9vS2RxuBNeDXQfWAWPgKvZAzty5yr7gVX6vRjqo5Pf61nZAAraHmkpXpSZBIh28AqrFkKjeZC1c4IZCQPrlKo3DNSm6Q9zN80jZBjFhy62L9xIwvS5JHKORiZAvh3zVaX8BPnK",
    "name": "Daniel Uhm",
    "description": "Daniel is currently a junior at USC majoring in Business Administration and minoring in Web Technologies and Applications. Some of his hobbies include football, mobile gaming, and catching up on tech news.",
    "created_at": "2015-04-21T22:25:12.000Z",
    "updated_at": "2015-04-23T06:39:44.000Z",
    "deleted_at": null,
    "fb": {
      "id": "10204989182542996",
      "first_name": "Shane",
      "gender": "male",
      "last_name": "Mileham",
      "link": "https://www.facebook.com/app_scoped_user_id/10204989182542996/",
      "locale": "en_US",
      "name": "Shane Mileham",
      "timezone": -7,
      "updated_time": "2015-04-15T23:25:33+0000",
      "verified": true,
      "picture": "images/daniel.png"
    }
  },
  {
    "id": 1,
    "fb_id": "10204989182542996",
    "fb_token": "CAAT2wuZAAvZB8BAFAsO7V4sSUzeAhjRbU8bZCCrwkGZAjp9BaZC9alAzEzOYXZBnUBsUTZBusGpY0i90bT0n7cnn9vS2RxuBNeDXQfWAWPgKvZAzty5yr7gVX6vRjqo5Pf61nZAAraHmkpXpSZBIh28AqrFkKjeZC1c4IZCQPrlKo3DNSm6Q9zN80jZBjFhy62L9xIwvS5JHKORiZAvh3zVaX8BPnK",
    "name": "Brian Ly",
    "description": "Brian is currently studying Economics and Web Applications and Technologies.   He is currently serving as a marketing research intern at Smartify. Some of his interests include basketball, hip-hop, and technology.",
    "created_at": "2015-04-21T22:25:12.000Z",
    "updated_at": "2015-04-23T06:39:44.000Z",
    "deleted_at": null,
    "fb": {
      "id": "10204989182542996",
      "first_name": "Shane",
      "gender": "male",
      "last_name": "Mileham",
      "link": "https://www.facebook.com/app_scoped_user_id/10204989182542996/",
      "locale": "en_US",
      "name": "Shane Mileham",
      "timezone": -7,
      "updated_time": "2015-04-15T23:25:33+0000",
      "verified": true,
      "picture": "images/brianl.jpg"
    }
  }
]);
    
    models.user.findAll().then(function(users){
        auth.mergeUsers(req, res, next, users).then(function(mergedUsers){
            res.status(200).json(mergedUsers);
        });
    });
});

// POST user
router.post('/', function(req, res, next){
    var fb_id = null, fb_token = null, name = null, description = null;

	if (req.body.fb_id){
        fb_id = req.body.fb_id;
    }
	if (req.body.fb_token){
        fb_token = req.body.fb_token;
    }
	if (req.body.name){
        name = req.body.name;
    }
	if (req.body.description){
        description = req.body.description;
    }

	models.user.create({
		fb_id: fb_id,
		fb_token: fb_token,
		name: name,
		description: description
	}).then(function(created_user){
        res.status(201).json(created_user);
    });
});

// GET user/:userid
router.get('/:userid', function(req, res, next){
    models.user.findOne({
        where: {
            id: req.params.userid
        }
    }).then(function(user){
        auth.mergeUser(req, res, next, user).then(function(mergedUser){
            res.status(200).json(mergedUser);
        });
    });
});

// POST user/update/:userid
router.post('/update/:userid', function(req, res, next){
    var userid = req.params.userid,
		promise_fb_id = null,
		promise_fb_token = null,
		promise_name = null,
		promise_description = null;

	if (req.body.fb_id){
        promise_fb_id = models.user.update({
            fb_id: req.body.fb_id
        },{
            where: {
                id: userid
            }
        });
    }

	if (req.body.fb_token){
        promise_fb_token = models.user.update({
            fb_token: req.body.fb_token
        },{
            where: {
                id: userid
            }
        });
    }

	if (req.body.name){
        promise_name = models.user.update({
            name: req.body.name
        },{
            where: {
                id: userid
            }
        });
    }

	if (req.body.description){
        promise_description = models.user.update({
            description: req.body.description
        },{
            where: {
                id: userid
            }
        });
    }

	q.all([promise_fb_id, promise_fb_token, promise_name, promise_description]).then(function(){
        models.user.findOne({
            where: {
                id: userid
            }
        }).then(function(updated_user){
            res.status(200).json(updated_user);
        });
    });
});

// POST user/destroy/:userid
router.post('/destroy/:userid', function(req, res, next) {
    models.user.findOne({
        where: {
            id: req.params.userid
        }
    }).then(function(user){
        models.user.destroy({
            where: {
                id: req.params.userid
            }
        }).then(function(){
            res.status(200).json(user);
        });
    });
});

// GET user/:userid/ideas
router.get('/:userid/ideas', function(req, res, next){
    models.user.findOne({
        where: {
            id: req.params.userid
        }
    }).then(function(user){
        user.getIdea().then(function(users){
            res.status(200).json(users);
        });
    });
});

// POST users/:userid/ideas
router.post(':userid/ideas', function(req, res, next){
    var promise_user = null,
		promise_idea = null;

    promise_user = models.user.findOne({
        where: {
            id: req.params.userid
        }
    });
    promise_idea = models.idea.findOne({
        where: {
            id: req.body.ideaid
        }
    });

    q.all([promise_user, promise_idea]).then(function(results){
        var user = results[0],
		idea = results[1];
        user.addIdea(idea);
        res.status(201).json(idea);
    });
});

// POST users/:userid/ideas/:ideaid/destroy
router.post('/:userid/ideas/:ideaid/destroy', function(req, res, next){
    var promise_user = null,
		promise_idea = null;

    promise_user = models.user.findOne({
        where: {
            id: req.params.userid
        }
    });
    promise_idea = models.idea.findOne({
        where: {
            id: req.body.ideaid
        }
    });

    q.all([promise_user, promise_idea]).then(function(results){
        var user = results[0],
		idea = results[1];
        user.removeIdea(idea);
        res.status(200).json(idea);
    });
});

// GET user/:userid/tags
router.get('/:userid/tags', function(req, res, next){
    models.user.findOne({
        where: {
            id: req.params.userid
        }
    }).then(function(user){
        user.getTag().then(function(users){
            res.status(200).json(users);
        });
    });
});

// POST users/:userid/tags
router.post(':userid/tags', function(req, res, next){
    var promise_user = null,
		promise_tag = null;

    promise_user = models.user.findOne({
        where: {
            id: req.params.userid
        }
    });
    promise_tag = models.tag.findOne({
        where: {
            id: req.body.tagid
        }
    });

    q.all([promise_user, promise_tag]).then(function(results){
        var user = results[0],
		tag = results[1];
        user.addTag(tag);
        res.status(201).json(tag);
    });
});

// POST users/:userid/tags/:tagid/destroy
router.post('/:userid/tags/:tagid/destroy', function(req, res, next){
    var promise_user = null,
		promise_tag = null;

    promise_user = models.user.findOne({
        where: {
            id: req.params.userid
        }
    });
    promise_tag = models.tag.findOne({
        where: {
            id: req.body.tagid
        }
    });

    q.all([promise_user, promise_tag]).then(function(results){
        var user = results[0],
		tag = results[1];
        user.removeTag(tag);
        res.status(200).json(tag);
    });
});

module.exports = router;

