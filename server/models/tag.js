var Sequelize = require('sequelize');
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);

module.exports = function (sequelize, DataTypes) {
    'use strict';
    var tag = sequelize.define('tag', {
		text: {
            type: Sequelize.STRING,
            allowNull: true
        }
	},{
        tableName: 'tag',
        underscored: true,
        paranoid: true,
        classMethods: {
            associate: function(models){
				tag.belongsToMany(models.user, {through: 'user_tag'});
            }
        }
    });
    return tag;
};