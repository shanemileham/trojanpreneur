var Sequelize = require('sequelize');
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.json')[env];
var sequelize = new Sequelize(config.database, config.username, config.password, config);

module.exports = function (sequelize, DataTypes) {
    'use strict';
    var idea = sequelize.define('idea', {
		name: {
            type: Sequelize.STRING,
            allowNull: true
        },
		description: {
            type: Sequelize.STRING,
            allowNull: true
        }
	},{
        tableName: 'idea',
        underscored: true,
        paranoid: true,
        classMethods: {
            associate: function(models){
				idea.belongsToMany(models.user, {through: 'idea_user'});
            }
        }
    });
    return idea;
};