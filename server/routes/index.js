/*jslint node: true, white: true*/

//Routes at: https://github.com/shanemileham/trojanpreneur/wiki/Database-API

var express = require('express');
var router = express.Router();
var models  = require('../models');

/* GET home page. */
router.get('/', function(req, res, next) {
    'use strict';

    res.render('index', { title: 'Express - Smileham' });
    res.end();
});





module.exports = router;